import React from 'react';
import ReactDOM from 'react-dom/client';
import './Styles/index.css';
import './Styles/smallDevices.css';
import './Styles/mediumDevices.css';
import './Styles/largeDevices.css';
import './Styles/biggerScreens.css';
import './Styles/languages.css';
import App from './App';
import { BrowserRouter } from 'react-router-dom';
import ContextComp from './Components/ContextComp';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <ContextComp>
        <App />
      </ContextComp>
    </BrowserRouter>
  </React.StrictMode>
);
