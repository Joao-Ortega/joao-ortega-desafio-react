import React, { useContext, useEffect, useState } from "react"
import UserContext from "../Context/UserContext"
import { getFromLocal } from "../Services/localStorage";
import UserInfos from "../Components/UserInfos";
import ReposInfos from "../Components/ReposInfos";
import Loading from "../Components/Loading";
import { useNavigate } from "react-router-dom";

export default function ProfilePage() {
  const { fetchUserRepos, repos } = useContext(UserContext);
  const [isLoading, setIsLoading] = useState(true);
  const [currentUser, setCurrentUser] = useState({});
  const navigate = useNavigate();
  
  useEffect(() => {
    getInfos()
  }, [])
  
  const getInfos = async () => {
    const user = getFromLocal("userInfos");
    if (!user) {
      navigate('/')
    }
    setCurrentUser(user)
    await fetchUserRepos(user.login);
    setTimeout(() => {
      setIsLoading(false)
    }, 500)
  }


  if (isLoading) {
    return <Loading />
  }
  return (
    <div>
      {repos.length && (
        <header>
          <div className="nav-bar">
            <img
              src="https://toppng.com/uploads/preview/git-repository-icon-github-repo-icon-svg-115534438372aunh6vf0u.png"
              alt="repositories icon"
            />
            <span className="span-title">Repositories</span>
            <span className="length">{repos.length}</span>
          </div>
        </header>
      )}
      <div className="main-content-container">
        <UserInfos user={currentUser} />
        <ReposInfos repositories={repos} />
      </div>
    </div>
  )
}
