import React, { useContext, useEffect, useState } from "react";
import UserContext from "../Context/UserContext";
import searchIcon from "../images/search-icon.svg";
import { clearLocal } from "../Services/localStorage";

export default function SearchPage() {
  const [username, setUsername] = useState('');
  const [emptyMessage, setEmptyMessage] = useState('');
  const { fetchGitUser, notFoundMsg } = useContext(UserContext);
  

  const handleChange = ({ target: { value } }) => {
    setUsername(value)
  }

  const handleFetchUser = ({ target: { value } }) => {
    const MIN_LENGTH = 1;

    if (username.length >= MIN_LENGTH) {
      fetchGitUser(username)
    } else {
      setEmptyMessage('Informe um nome de usuário válido do Github')
      setTimeout(() => {
        setEmptyMessage('')
      }, 3000)
    }
  }

  useEffect(() => {
    clearLocal()
  }, [])

  return (
    <div className="main-content">
      <div
        className="input-group mb-3 user-input"
      >
        <span className="search-title">Buscar Repositório no github</span>
        <div className="input-container">
          <input
            type="text"
            className="form-control"
            placeholder="digite o nome do usuário"
            onChange={ handleChange }
            style={{ fontStyle: "italic" }}
          />
          <button
            type="button"
            className="btn btn-secondary search-btn"
            name="search-btn"
            onClick={ handleFetchUser }
          >
            <img
              className="search-icon-img"
              src={searchIcon}
              alt="search icon"
            />
            Buscar
          </button>
        </div>
        { emptyMessage && <span className="empty-msg">{emptyMessage}</span> }
        { notFoundMsg.includes("404") && (
          <span
            className="empty-msg"
          >
            Usuário não encontrado no Github. Verifique se você digitou o nome corretamente.
          </span>
        )}
      </div>
    </div>
  )
}
