export const saveOnLocal = (key, data) => localStorage.setItem(key, JSON.stringify(data));
export const clearLocal = () => localStorage.clear();
export const getFromLocal = (key) => JSON.parse(localStorage.getItem(key));