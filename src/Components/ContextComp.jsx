import axios from "axios";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import UserContext from "../Context/UserContext";
import { saveOnLocal } from "../Services/localStorage";
import { userFetch } from "../URLs/urls";

export default function ContextComp({ children }) {
  const [userFromApi, setUserFromApi] = useState({});
  const [notFoundMsg, setNotFoundMsg] = useState('');
  const [repos, setRepos] = useState([]);
  const navigate = useNavigate();

  const fetchGitUser = (username) => {
    axios.get(`${userFetch}${username}`)
      .then(({ data:
        { name, login, email, followers, following, public_repos, avatar_url, bio}
      }) => {
        const userObj = { name, login, email, followers, following, public_repos, avatar_url, bio}
        setUserFromApi(userObj)
        saveOnLocal('userInfos', userObj)
        navigate("/profile")
      })
      .catch(({message}) => {
        setNotFoundMsg(message)
        setTimeout(() => setNotFoundMsg(''), 3200)
      })
  }

  const fetchUserRepos = (username) => {
    const REPOS_URL = `https://api.github.com/users/${username}/repos`;
    axios.get(REPOS_URL)
      .then(({ data }) => setRepos(data))
      .catch((error) => error);
  }
  
  const state = {
    userFromApi,
    notFoundMsg,
    repos,
    fetchGitUser,
    fetchUserRepos,
  }

  return (
    <UserContext.Provider value={ state }>
      { children }
    </UserContext.Provider>
  )
}
