import React from "react";
import { months } from "../Services/utils";
import starIcon from "../images/star-icon.svg";
import { ReactComponent as ForkLogo } from "../images/forked.svg";
import { ReactComponent as LicenseLogo } from "../images/license-logo.svg";

export default function ReposInfos({ repositories }) {
  const formatDate = (time) => {
    const date = new Date(time);
    const month = months[date.getMonth()];
    const day = date.getDate();
    const year = date.getFullYear();
    return `${day} ${month} ${year}`
  }

  return (
    <div className="repos-container">
      {repositories.map((repo) => (
        <div key={repo.name} className="repo-card">
          <div className="title-repo">
            <a
              href={repo.html_url}
              target="_blank"
              rel='noreferrer'
              className="link-to-repo"         
            >
              {repo.name}
            </a>
            <span
              className="visibility"
            >
              {`${repo.visibility[0].toUpperCase()}${repo.visibility.slice(1)}`}
            </span>
          </div>
          { repo.fork && <p className="span-repo-infos">Forked</p> }
          { repo.description && <p className="span-repo-infos">{repo.description}</p> }
          <div className="container-infos">
            { repo.language && <div className={`lang ${repo.language}`} />  }
            { repo.language && <span className="language">{repo.language}</span> }
            { repo.forks > 0 && (
              <div className="repo-details">
                <ForkLogo />
                <span className="span-repo-infos">{repo.forks}</span>
              </div>
            )}
            { repo.license && (
              <div className="repo-details">
                <LicenseLogo />
                <span className="span-repo-infos">{repo.license.name}</span>
              </div>
            )}
            { repo.updated_at && (
              <span className="span-repo-infos">{`Updated on ${formatDate(repo.updated_at)}`}</span>
            )}
          </div>
          { repo.stargazers_count > 0 && (
            <div className="stars-from-repo">
              <img src={starIcon} alt="star" className="stars" />
              <span>{repo.stargazers_count}</span>
            </div>
          ) }
        </div>
      ))}
    </div>
  )
}
