import React from "react";

export default function Loading() {
  return (
    <div class="spinner-border text-dark loading" role="status">
      <span class="sr-only" />
    </div>
  );
}
