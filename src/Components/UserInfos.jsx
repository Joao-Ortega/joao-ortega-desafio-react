import React, { useContext, useEffect, useState } from "react";
import UserContext from "../Context/UserContext";
import starIcon from "../images/star-icon.svg";
import { ReactComponent as FollowersLogo } from "../images/user-group.svg";
import { getFromLocal, saveOnLocal } from "../Services/localStorage";

export default function UserInfos({ user }) {
  const { repos } = useContext(UserContext);
  const [isFollowing, setIsFollowing] = useState(false);

  const countStarsFromRepos = () => {
    const totalStars = repos.reduce((acc, curr) => acc + curr.stargazers_count, 0);
    return <span className="span-followers">{totalStars}</span>
  }

  const followUser = () => {
    setIsFollowing(!isFollowing)
    saveOnLocal('follow-status', !isFollowing)
  }

  useEffect(() => {
    const alreadyFollow = getFromLocal('follow-status');
    if (alreadyFollow) {
      setIsFollowing(alreadyFollow)
    }
  }, [])

  return (
    <div className="user-infos-container">
        <div className="image-and-name">
          <img src={user.avatar_url} alt="user profile" />
          <div className="names">
            <h5 id="username">{user.name}</h5>
            <p>{user.login}</p>
          </div>
        </div>
        <div className="bio-informations">
          <button
            type="button"
            className={isFollowing ? "follow-btn-active" : "follow-btn"}
            onClick={ followUser }
            >
            { isFollowing ? 'Following' : 'Follow' }
          </button>
          { user.bio && <p className="bio">{user.bio}</p> }
          <div className="followers-infos-container">
            <FollowersLogo />
            <span className="span-followers">{`${user.followers} followers`}</span>
            {'-'}
            <span className="span-followers">{`${user.following} following`}</span>
            {'-'}
            <img
              src={starIcon}
              alt="star icon"
              height="22"
              width="15"
            />
            { countStarsFromRepos() }
            { user.email && <span className="span-followers">{user.email}</span> }
          </div>
        </div>
      </div>
  )
}
