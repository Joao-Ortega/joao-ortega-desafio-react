import { Route, Routes } from "react-router-dom";
import ProfilePage from "./Pages/ProfilePage";
import SearchPage from "./Pages/SearchPage";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={ <SearchPage /> } />
        <Route path="/profile" element={ <ProfilePage /> } />
      </Routes>
    </div>
  );
}

export default App;
